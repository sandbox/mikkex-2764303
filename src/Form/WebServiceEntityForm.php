<?php

namespace Drupal\webservice_watcher\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WebServiceEntityForm.
 *
 * @package Drupal\webservice_watcher\Form
 */
class WebServiceEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $web_service_entity = $this->entity;
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $web_service_entity->label(),
      '#description' => $this->t("Label for the Web service entity."),
      '#required' => TRUE,
    );
    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#maxlength' => 255,
      '#default_value' => $web_service_entity->getEndpoint(),
      '#description' => $this->t("Web service URL endpoint"),
      '#required' => TRUE,
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $web_service_entity->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\webservice_watcher\Entity\WebServiceEntity::load',
      ),
      '#disabled' => !$web_service_entity->isNew(),
    );
    $form['information'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Information about this web service'),
      '#rows' => 5,
      '#default_value' => $web_service_entity->getInformation(),
      '#description' => $this->t("Web service information"),
      '#required' => FALSE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $web_service_entity = $this->entity;
    $status = $web_service_entity->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Web service entity.', [
          '%label' => $web_service_entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Web service entity.', [
          '%label' => $web_service_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($web_service_entity->urlInfo('collection'));
  }

}
