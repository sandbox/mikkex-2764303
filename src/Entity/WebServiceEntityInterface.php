<?php

namespace Drupal\webservice_watcher\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Web service entity entities.
 */
interface WebServiceEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.

  public function getEndpoint();
  public function getInformation();

}
