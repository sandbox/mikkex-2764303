<?php

namespace Drupal\webservice_watcher\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Web service entity entity.
 *
 * @ConfigEntityType(
 *   id = "web_service_entity",
 *   label = @Translation("Web service entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\webservice_watcher\WebServiceEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\webservice_watcher\Form\WebServiceEntityForm",
 *       "edit" = "Drupal\webservice_watcher\Form\WebServiceEntityForm",
 *       "delete" = "Drupal\webservice_watcher\Form\WebServiceEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\webservice_watcher\WebServiceEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "web_service_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/web_service_entity/{web_service_entity}",
 *     "add-form" = "/admin/config/web_service_entity/add",
 *     "edit-form" = "/admin/config/web_service_entity/{web_service_entity}/edit",
 *     "delete-form" = "/admin/config/web_service_entity/{web_service_entity}/delete",
 *     "collection" = "/admin/config/web_service_entity"
 *   }
 * )
 */
class WebServiceEntity extends ConfigEntityBase implements WebServiceEntityInterface {

  /**
   * The Web service entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Web service entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The endpoint URL.
   *
   * @var string
   */
  public $endpoint;


  /**
   * Information about web service.
   *
   * @var string
   */
  public $information;

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->endpoint;
  }

  /**
   * {@inheritdoc}
   */
  public function getInformation() {
    return $this->information;
  }
}
