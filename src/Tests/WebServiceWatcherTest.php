<?php
namespace Drupal\webservice_watcher\Tests;

use Drupal\simpletest\WebTestBase;

/**
* Tests the Drupal 8 demo module functionality
*
* @group webservice_watcher
*/
class WebServiceWatcherTest extends WebTestBase {


  /**
  * Modules to install.
  *
  * @var array
  */
  public static $modules = array('webservice_watcher');

  /**
  * A simple user with 'access content' permission
  */
  private $user;

  /**
  * Perform any initial set up tasks that run before every test method
  */
  public function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser(array('administer site configuration'));
    }


  /**
   * Tests that the 'admin/config/webservice_watcher' path returns the right content
   */
  public function testWebServiceWatcherPageExists() {
    $this->drupalLogin($this->user);

    $this->drupalGet('/admin/config/webservice_watcher');
    $this->assertResponse(200);
    //$this->assertText('WEB SERVICE ENTITY');
  }
}